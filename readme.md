# Terraform Container

Including:

* [TFSwitch](https://github.com/warrensbox/terraform-switcher/)
* [TFLint](https://github.com/terraform-linters/tflint)
* [infracost](https://github.com/infracost/infracost)
* [yor](https://github.com/bridgecrewio/yor)
* git-crypt / Gnupg
