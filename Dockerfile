FROM infracost/infracost:0.10.34

FROM quay.io/terraform-docs/terraform-docs:0.17.0

FROM hashicorp/terraform:1.7.4

FROM bridgecrew/yor:0.1.190

FROM alpine:3.19
LABEL maintainer=<strowi@hasnoname.de>

ENV INFRACOST_API_KEY=""
ENV INFRACOST_CURRENCY="EUR"
ENV INFRACOST_SKIP_UPDATE_CHECK=true

COPY --from=0 /usr/bin/infracost /usr/bin
COPY --from=0 /scripts /scripts
RUN infracost --version

COPY --from=1 /usr/local/bin/terraform-docs /usr/bin/terraform-docs
RUN terraform-docs --version

COPY --from=2 /bin/terraform /bin/terraform

COPY --from=3 /usr/bin/yor /bin/yor
RUN yor --version

# renovate: datasource=github-releases depName=warrensbox/terraform-switcher versioning=newest
ENV TFSWITCH_VERSION="0.13.1218"
RUN apk --update --no-cache add \
    bash \
    openssh-client \
    git \
    git-crypt \
    gnupg \
    jq \
    curl \
    libc6-compat \
  && curl -L https://github.com/warrensbox/terraform-switcher/releases/download/${TFSWITCH_VERSION}/terraform-switcher_${TFSWITCH_VERSION}_linux_amd64.tar.gz \
    | tar xz tfswitch -C /usr/bin/ \
  &&  tfswitch --version

COPY src/usr/bin/ /usr/bin
